using System;

namespace quiz1
{
    public class AgeCalc
    {
        public DateTime birthDate { get; }
        public DateTime dateNow { get; }

        public AgeCalc(int day, int month, int year)
        {
            birthDate = new DateTime(year, month, day);
            dateNow = new DateTime();
            dateNow = DateTime.Today;
        }

        public int Age()
            => dateNow.Year - birthDate.Year ;



    }
}